import { Component, EventEmitter, Input, Output, forwardRef } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { INPUT_TYPE, StyleRadio } from './base-input.util';
// export const CUSTOM_CONROL_VALUE_ACCESSOR: any = {
//   provide: NG_VALUE_ACCESSOR,
//   useExisting: forwardRef(() => BaseInputComponent),
//   multi: true,
// };


@Component({
  selector: 'base-input',
  templateUrl: './base-input.component.html',
  styleUrls: ['./base-input.component.scss'],
  // providers : [CUSTOM_CONROL_VALUE_ACCESSOR]
})
export class BaseInputComponent {
  @Input() label: string = '';
  @Input() iconName : string = 'text';
  @Input() radioValue : string ="";
  @Input() formControl_ !: FormControl ;
  TYPE = INPUT_TYPE
  @Input() INPUT_TYPE  = INPUT_TYPE.text;
  styleRadio = StyleRadio;
  constructor(){
    
  }
}
