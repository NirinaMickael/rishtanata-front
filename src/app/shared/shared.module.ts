import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseInputComponent } from './components/base-input/base-input.component';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const C = [
  BaseInputComponent
]

@NgModule({
  declarations: [
    ...C
  ],
  imports: [
    CommonModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    ...C
  ]
})
export class SharedModule { }
