import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { INPUT_TYPE } from 'src/app/shared/components/base-input/base-input.util';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  form !: FormGroup;
  TYPE = INPUT_TYPE;
  constructor(private fb: FormBuilder){}
  ngOnInit(): void {
    this.form = this.fb.group({
      mail: ['', Validators.required],
    });
  ;}

  get control(){
    return this.form.controls as any
  }
  test(){
    console.log(this.form.value)
  }
}
