import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AuthComponent } from './auth.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

const routes: Routes = [
  {
    path:'',
    component:AuthComponent,
    children :[
      { path: "", component: SigninComponent },
      {path:'forgot-password',component:ForgotPasswordComponent},
      { path: "resetpassword/:token", component: SigninComponent },
      {path:"sign-up",component:SignupComponent},
      {path:"**",component:SigninComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
