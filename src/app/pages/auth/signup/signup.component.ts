import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { INPUT_TYPE } from 'src/app/shared/components/base-input/base-input.util';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {
  form !: FormGroup;
  TYPE = INPUT_TYPE;
  constructor(private fb: FormBuilder){}
  ngOnInit(): void {
    this.form = this.fb.group({
      gender: ['', Validators.required],
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      date:['',Validators.required],
      contact:['',Validators.required,]
    });
  ;}

  get control(){
    return this.form.controls as any
  }
  test(){
    console.log(this.form.value)
  }
}
