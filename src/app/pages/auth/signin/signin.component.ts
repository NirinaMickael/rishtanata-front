import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { INPUT_TYPE } from 'src/app/shared/components/base-input/base-input.util';
enum DisplayMode {
  Login,
  Forgot,
  Reset
}

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  form !: FormGroup;
  TYPE = INPUT_TYPE;
  constructor(private fb: FormBuilder){}
  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password : ['',Validators.required]
    });
  ;}

  get control(){
    return this.form.controls as any
  }
  test(){
    console.log(this.form.value)
  }

}
