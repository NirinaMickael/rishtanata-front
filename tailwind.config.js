module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  mode: 'aot',
  theme: {
    extend: {
      colors: {
        'primary-100':'var(--primary-100)',
        'primary-200':'var(--primary-200)',
        'primary-300':'var(--primary-300)',
        'secondary':'var(--color-secondary)',
      }
    },
  },
  plugins: [
  ],
}
